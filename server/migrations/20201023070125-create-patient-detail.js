'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('PatientDetails', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      visitDate: {
        type: Sequelize.DATE
      },
      visitTime: {
        type: Sequelize.TIME
      },
      expectedVisitDate: {
        type: Sequelize.DATE
      },
      dob: {
        type: Sequelize.DATE
      },
      sex: {
        type: Sequelize.BOOLEAN
      },
      actualTime: {
        type: Sequelize.TIME
      },
      height: {
        type: Sequelize.STRING
      },
      weight: {
        type: Sequelize.STRING
      },
      temperature: {
        type: Sequelize.STRING
      },
      heartRate: {
        type: Sequelize.STRING
      },
      systolicBloodPressure: {
        type: Sequelize.STRING
      },
      diastolicBloodPressure: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP()')
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()')
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('PatientDetails');
  }
};