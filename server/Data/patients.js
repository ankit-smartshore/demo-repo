const patients = [{
        "firstName": "James",
        "lastName": "Morrow",
        "email": "condimentum.eget.volutpat@dictumcursusNunc.edu",
        "active": "0",
        "enrolled": "0",
        "enrollDate": "2020-09-12 04:24:56"
    },
    {
        "firstName": "Amber",
        "lastName": "Peck",
        "email": "vitae@augueeutellus.com",
        "active": "0",
        "enrolled": "1",
        "enrollDate": "2020-05-14 21:19:35"
    },
    {
        "firstName": "Wyoming",
        "lastName": "Walters",
        "email": "a.mi@Integertinciduntaliquam.edu",
        "active": "0",
        "enrolled": "0",
        "enrollDate": "2020-09-19 08:00:55"
    },
    {
        "firstName": "Hayfa",
        "lastName": "Wiggins",
        "email": "tincidunt.tempus.risus@et.org",
        "active": "0",
        "enrolled": "0",
        "enrollDate": "2020-01-21 09:23:22"
    },
    {
        "firstName": "Yael",
        "lastName": "Holt",
        "email": "purus.gravida@laoreetipsum.com",
        "active": "1",
        "enrolled": "1",
        "enrollDate": "2020-08-29 20:01:46"
    },
    {
        "firstName": "Ocean",
        "lastName": "Allison",
        "email": "ante.iaculis@gravida.co.uk",
        "active": "1",
        "enrolled": "1",
        "enrollDate": "2020-06-15 21:16:20"
    },
    {
        "firstName": "Jordan",
        "lastName": "Edwards",
        "email": "vulputate.mauris@iaculisnec.net",
        "active": "1",
        "enrolled": "1",
        "enrollDate": "2020-07-03 19:06:14"
    },
    {
        "firstName": "Mariko",
        "lastName": "Ayers",
        "email": "euismod@convallisante.ca",
        "active": "1",
        "enrolled": "0",
        "enrollDate": "2020-10-16 20:22:17"
    },
    {
        "firstName": "Lawrence",
        "lastName": "Mcmillan",
        "email": "interdum@loremfringillaornare.com",
        "active": "0",
        "enrolled": "1",
        "enrollDate": "2020-01-31 03:22:56"
    },
    {
        "firstName": "Brynn",
        "lastName": "Hooper",
        "email": "erat.nonummy@bibendumDonec.com",
        "active": "1",
        "enrolled": "1",
        "enrollDate": "2019-11-12 19:36:54"
    },
    {
        "firstName": "Wade",
        "lastName": "Mueller",
        "email": "nonummy.ultricies@dapibusligulaAliquam.edu",
        "active": "0",
        "enrolled": "0",
        "enrollDate": "2020-06-09 04:53:09"
    },
    {
        "firstName": "Vanna",
        "lastName": "Woods",
        "email": "Aliquam.erat.volutpat@rhoncusProin.edu",
        "active": "0",
        "enrolled": "1",
        "enrollDate": "2020-04-21 00:07:58"
    },
    {
        "firstName": "Talon",
        "lastName": "Jimenez",
        "email": "amet.massa@nec.edu",
        "active": "0",
        "enrolled": "1",
        "enrollDate": "2020-05-12 01:00:57"
    },
    {
        "firstName": "Honorato",
        "lastName": "Miles",
        "email": "mi.enim.condimentum@nonloremvitae.edu",
        "active": "1",
        "enrolled": "0",
        "enrollDate": "2020-05-20 03:49:18"
    },
    {
        "firstName": "Zane",
        "lastName": "Patrick",
        "email": "feugiat.non@Pellentesqueultricies.org",
        "active": "0",
        "enrolled": "0",
        "enrollDate": "2019-12-24 14:52:24"
    },
    {
        "firstName": "Cadman",
        "lastName": "Graves",
        "email": "mi@Mauris.ca",
        "active": "1",
        "enrolled": "0",
        "enrollDate": "2019-10-25 21:54:29"
    },
    {
        "firstName": "Lamar",
        "lastName": "Hardin",
        "email": "porttitor@atsemmolestie.co.uk",
        "active": "0",
        "enrolled": "1",
        "enrollDate": "2020-02-19 11:24:12"
    },
    {
        "firstName": "Brielle",
        "lastName": "Dennis",
        "email": "Duis.cursus@quispedePraesent.net",
        "active": "0",
        "enrolled": "0",
        "enrollDate": "2020-09-16 08:27:46"
    },
    {
        "firstName": "Henry",
        "lastName": "Boone",
        "email": "ipsum.porta@quispede.ca",
        "active": "1",
        "enrolled": "0",
        "enrollDate": "2020-04-07 04:00:47"
    },
    {
        "firstName": "Xena",
        "lastName": "Strong",
        "email": "malesuada.Integer.id@volutpatNulla.edu",
        "active": "1",
        "enrolled": "1",
        "enrollDate": "2020-04-10 17:57:50"
    },
    {
        "firstName": "Demetria",
        "lastName": "Dunn",
        "email": "non.lacinia.at@sagittis.org",
        "active": "0",
        "enrolled": "0",
        "enrollDate": "2019-12-23 10:02:32"
    },
    {
        "firstName": "Joel",
        "lastName": "Chandler",
        "email": "semper.rutrum.Fusce@dui.com",
        "active": "0",
        "enrolled": "1",
        "enrollDate": "2020-10-24 11:48:27"
    },
    {
        "firstName": "Quintessa",
        "lastName": "Beck",
        "email": "amet.lorem@pedeSuspendissedui.edu",
        "active": "1",
        "enrolled": "1",
        "enrollDate": "2019-12-18 06:59:23"
    },
    {
        "firstName": "Carolyn",
        "lastName": "Hunter",
        "email": "non@malesuadavelconvallis.net",
        "active": "1",
        "enrolled": "0",
        "enrollDate": "2020-07-26 22:37:30"
    },
    {
        "firstName": "Adena",
        "lastName": "Hines",
        "email": "interdum.Curabitur.dictum@mi.ca",
        "active": "0",
        "enrolled": "0",
        "enrollDate": "2020-09-14 10:18:27"
    },
    {
        "firstName": "Leroy",
        "lastName": "Carr",
        "email": "mollis@massaVestibulum.edu",
        "active": "0",
        "enrolled": "0",
        "enrollDate": "2020-03-26 19:00:56"
    },
    {
        "firstName": "Barbara",
        "lastName": "Reid",
        "email": "facilisi.Sed.neque@est.net",
        "active": "0",
        "enrolled": "1",
        "enrollDate": "2020-07-13 07:07:43"
    },
    {
        "firstName": "Hillary",
        "lastName": "Stevens",
        "email": "magna.Cras@Vivamussitamet.net",
        "active": "1",
        "enrolled": "1",
        "enrollDate": "2019-10-28 13:27:34"
    },
    {
        "firstName": "Kylynn",
        "lastName": "Jennings",
        "email": "tempor.augue.ac@Nuncacsem.co.uk",
        "active": "1",
        "enrolled": "0",
        "enrollDate": "2020-04-04 14:12:50"
    },
    {
        "firstName": "Ross",
        "lastName": "Watts",
        "email": "metus.In.nec@ipsum.org",
        "active": "1",
        "enrolled": "1",
        "enrollDate": "2020-03-20 01:25:27"
    },
    {
        "firstName": "Elmo",
        "lastName": "Henderson",
        "email": "enim.Curabitur.massa@diamPellentesque.co.uk",
        "active": "0",
        "enrolled": "0",
        "enrollDate": "2020-05-18 15:25:16"
    },
];


module.exports = patients