'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Patient extends Model {

  };
  Patient.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    active: DataTypes.BOOLEAN,
    enrolled: DataTypes.BOOLEAN,
    enrollDate: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Patients',
    timestamps: true
  });


  Patient.associate = models => {
    Patient.belongsTo(models.Doctors,{
      as: 'doctor',
      foreignKey: {
        name: 'DoctorsId'
      }
    })

    Patient.hasOne(models.PatientDetails, {
      as: 'patientdetail',
      foreignKey: {
        name: 'PatientId'
      }
    });
  };

  return Patient;
};