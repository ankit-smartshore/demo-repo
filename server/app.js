require("dotenv").config();
const express = require("express");
const app = express();
const cors = require('cors')
const port = process.env.PORT || 4000;

app.use(cors())
app.use(express.json());

// Api routes



const db = require("./models");

db.sequelize
    .authenticate()
    .then(()=> console.log("connection established"))
    .catch((err)=> console.log("Unable to connect to database", err))

db.sequelize.sync({
    // force: true
})
.then(() => {
    console.log("Connection Established");
})
.catch((err) =>{
    console.log("Unable to connect to db", err)
})



const authRouter = require("./routes/auth.routes");
const patientRouter = require("./routes/patient.routes");
app.use("/api/auth", authRouter);
app.use("/api/patient", patientRouter);


app.listen(port, () => {
    console.log("server up and running on PORT :", port);
});
