import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Patient from '../views/Patient/detail'
import AddPatient from '../views/Patient/addPatient'

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta:{
      layout: "defaultlayout"
    }
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      requiresVisitor: true,
      layout: "loginLayout"
    }
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
    meta: {
      requiresVisitor: true,
      layout: "loginLayout"
    }
  },
  {
    path: "/patient/new",
    name: "NewPatient",
    component: AddPatient,
    meta: {
      layout: "defaultlayout"
    }
  },
  {
    path: "/patient/:id",
    name: "Patient",
    component: Patient,
    meta: {
      layout: "defaultlayout"
    }
  },
  
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  var isLoggedIn = localStorage.getItem("jwt_token") && localStorage.getItem('isLoggedIn');
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!isLoggedIn) {
      next({
        path: '/login',
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    if (isLoggedIn) {
      next({
        path: '/',
      })
    } else {
      next()
    }
  } else {
    next()
  }

})

export default router
