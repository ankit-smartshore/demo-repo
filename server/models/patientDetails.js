'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PatientDetail extends Model {

  };
  PatientDetail.init({
    visitDate: DataTypes.DATE,
    visitTime: DataTypes.TIME,
    expectedVisitDate: DataTypes.DATE,
    dob: DataTypes.DATE,
    sex: DataTypes.BOOLEAN,
    actualTime: DataTypes.TIME,
    height: DataTypes.STRING,
    weight: DataTypes.STRING,
    temperature: DataTypes.STRING,
    heartRate: DataTypes.STRING,
    systolicBloodPressure: DataTypes.STRING,
    diastolicBloodPressure: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'PatientDetails',
    timestamps: true
  });

  return PatientDetail;
};