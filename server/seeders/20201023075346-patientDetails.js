'use strict';

const _patientDetails = require('../Data/patientDetails')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const PatientsID = await queryInterface.sequelize.query(
      `SELECT id from Patients;`
    );

    function addKeyValue(obj, key, data) {
      obj[key] = data;
    }

    const TextRow = PatientsID[0];

    _patientDetails.map(function (patient, index) {
      return addKeyValue(patient, 'PatientId', TextRow[index].id);
    });

    await queryInterface.bulkInsert('PatientDetails', _patientDetails, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};