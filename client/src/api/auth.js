import client from './client'

export default {
    login(data) {
        return client.withoutAuth().post('/auth/login', null, data, { "Access-Control-Allow-Origin": "*", crossorigin: true })
    },
    register(data) {
        return client.withoutAuth().post('/auth/signup', null, data, { "Access-Control-Allow-Origin": "*", crossorigin: true })
    }
}