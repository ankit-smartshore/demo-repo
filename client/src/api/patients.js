import client from './client'

export default {
  getPatients(data) {
    return client.withoutAuth().get('/patient', null, data, {
      "Access-Control-Allow-Origin": "*",
      crossorigin: true
    })
  },
  getPatientById(id) {
    return client.withoutAuth().get(`/patient/${id}`, {
      "Access-Control-Allow-Origin": "*",
      crossorigin: true
    })
  }
}