import router from "@/router";
import patients from '@/api/patients.js'

const state = {
  patients: null,
  currentPatient:null
};

const mutations = {
  setPatients(state, payload) {
    console.log(payload)
    state.patients = payload
  },
  setPatient(state, payload) {
    console.log(payload)
    state.currentPatient = payload
  },
};

const actions = {
  getPatients(context) {
    patients.getPatients().then(response => {
      context.commit('setPatients', response.data)
    })
  },
  getPatientById(context, id) {
    patients.getPatientById(id).then(response => {
      context.commit('setPatient', response.data)
    })
  },
}
const getters = {
  allPatients(state){
    return state.patients
  },
  singlePatient(state) {
    return state.currentPatient
  }
};

export default {
  state,
  mutations,
  actions,
  getters,
};