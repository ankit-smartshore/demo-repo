import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import DefaultLayout from "./layouts/defaultLayout";
import LoginLayout from "./layouts/loginLayout";

Vue.config.productionTip = false
Vue.component("defaultlayout", DefaultLayout);
Vue.component("loginLayout", LoginLayout);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
