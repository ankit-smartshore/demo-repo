'use strict';

const _patients = require('../Data/patients')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const DoctorsID = await queryInterface.sequelize.query(
      `SELECT id from Doctors;`
    );

    function addKeyValue(obj, key, data) {
      obj[key] = data;
    }

    const TextRow = DoctorsID[0];
    
    _patients.map(function (patient) {
      return addKeyValue(patient, 'DoctorsId', TextRow[Math.floor(Math.random() * TextRow.length)].id);
    });
    
    await queryInterface.bulkInsert('Patients', _patients, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};