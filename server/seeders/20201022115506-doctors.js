'use strict';

const _doctors = require('../Data/doctors')
const bcrypt = require("bcrypt");

module.exports = {
  up: async (queryInterface, Sequelize) => {

    function encryptPass(doctor, password) {
      doctor[password] = bcrypt.hashSync(doctor.password, 8)
    }

    _doctors.map(function (doctor) {
      return encryptPass(doctor, 'password')
    });
  
    await queryInterface.bulkInsert('Doctors', _doctors, {});

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
