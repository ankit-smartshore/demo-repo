const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const db = require("../models");
const Doctor = db.Doctors;
const Op = db.Op;

exports.signup = (req, res) => {

  // console.log('reqreq', req.body)

  const doctor = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  };

  // Save user to database
  Doctor.create(doctor)
    .then(doctor => {
        res.send({
          message: "doctor was registered successfully!"
        })
      }).catch(err => {
        // console.log(err)
      res.status(500).send({
        message: err
      });
    });
};

exports.signin = (req, res) => {
  Doctor.findOne({
      where: {
        email: req.body.email
      }
    })
    .then(doctor => {
      if (!doctor) {
        return res.status(404).send({
          message: "Doctor Not found."
        });
      }

      let passwordIsValid = bcrypt.compareSync(
        req.body.password,
        doctor.password
      );

      if (!passwordIsValid) {
        res.status(401).send({
          message: "Invalid Password!"
        });
      }

      let token = jwt.sign({
        id: doctor.id
      }, process.env.JWT_KEY, {
        expiresIn: 86400 // 24 hours
      });


      res.status(200).send({
        id: doctor.id,
        firstName: doctor.firstName,
        lastName: doctor.lastName,
        email: doctor.email,
        accessToken: token
      });

      // let authorities = [];
      // user.getRoles().then(roles => {
      //   for (let i = 0; i < roles.length; i++) {
      //     authorities.push("ROLE_" + roles[i].name.toUpperCase());
      //   }

      //   res.status(200).send({
      //     id: user.id,
      //     username: user.username,
      //     email: user.email,
      //     roles: authorities,
      //     accessToken: token
      //   });
      // });
    })
    .catch(err => {
      res.status(500).send({
        message: err.message
      });
    });
};