import auth from "@/api/auth";
import router from "@/router";
const state = {
  userData: {
    id: localStorage.getItem("id") ?
      localStorage.getItem("id") :
      null,
    firstName: localStorage.getItem("firstName") ?
      localStorage.getItem("firstName") :
      null,
    lastName: localStorage.getItem("lastName") ?
      localStorage.getItem("lastName") :
      null,
    email: localStorage.getItem("email") ?
      localStorage.getItem("email") :
      null,
  },
  isLoggedIn: localStorage.getItem("isLoggedIn") ?
    localStorage.getItem("isLoggedIn") :
    false,
  error: null
};

const mutations = {
  setUser(state, payload) {
    state.userData = {
      id: payload.data.id,
      firstName: payload.data.firstName,
      lastName: payload.data.lastName,
      email: payload.data.email,
    }
  },
  setIsLoggedIn(state, payload) {
    state.isLoggedIn = payload;
  },
  setError(state, payload) {
    state.error = payload;
  },
};

const actions = {
    login(context, data) {
      auth.login(data).then((response) => {
        if (response.status !== 200) {
          context.commit("setError", "something went wromng")
        }
        context.commit("setUser", response);
        context.commit("setIsLoggedIn", true);
        localStorage.setItem("isLoggedIn", true);
        localStorage.setItem("jwt_token", response.data.accessToken);
        localStorage.setItem("userDetails", {
          id: response.data.firstName,
          firstName: response.data.firstName,
          lastName: response.data.lastName,
          email: response.data.email,
        });
        router.push("/");
      }).catch((err) => console.error('woops', err))
    },
    register(context, data) {
      auth.register(data).then((response) => {
        // console.log("response", response);
        router.push("/login");
      }).catch((error) => console.log('woops', error))
    },
    logout(context) {
      localStorage.clear();
      context.commit("setIsLoggedIn", false);
      router.push("/login");
    },
  }
  
 const getters = {
    isLoggedIn(state) {
      return state.isLoggedIn;
    },
    getError(state) {
      return state.error;
    },
  };

export default {
  state,
  mutations,
  actions,
  getters,
};