const router = require("express").Router();
const { checkToken } = require("../middlewares/token_validation");
const patientContoller = require("../controllers/patient.controller");

router.post("/", checkToken, patientContoller.create);
router.get("/", patientContoller.findAll);
router.get("/active", patientContoller.findAllActive);
router.get("/inactive", patientContoller.findAllInActive);
router.get("/:id", patientContoller.findOne);
router.put("/:id", checkToken, patientContoller.update);
router.delete("/:id", checkToken, patientContoller.delete);
router.delete("/", checkToken, patientContoller.deleteAll);

module.exports = router;