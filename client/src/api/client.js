import axios from 'axios'
import store from '@/store'
const baseurl = 'http://localhost:5000/api'

const client = {
  auth: false,
  withAuth() {
    client.auth = true
    return client
  },
  withoutAuth() {
    client.auth = false
    return client
  }
}
const types = ['get', 'post', 'put', 'delete']
types.forEach((verb) => {
  return new Promise(() => {
    client[verb] = (url, params = {}, data = {}, headers = {}) => {
      return axios({
        method: verb,
        url: baseurl + url,
        params,
        data,
        headers: client.auth ? Object.assign({}, { 'Authorization': `Bearer ${localStorage.getItem('jwt_token')}` }, headers) : Object.assign({}, { 'Authorization': `x-api-key YToyOntzOjExOiJpbnRlcmZhY2VJZCI7czoxNDoiZGVsdGFyZXMwMDAxMjMiO3M6MTI6ImludGVyZmFjZUtleSI7czozMjoiMzgxMWQzZjFiZjMwMmJiOWE4ZTlkNjU5MzNlMjAxMGMiO30=` }, headers)
      }).then((res) => {
        // console.log('resres', res)
        client.auth = false
        if(res.data.session=='INACTIVE' && process.env.NODE_ENV =='production'){
          store.dispatch('auth/sessionExpired')
        }
        return Promise.resolve(res)
      }).catch((error) => {
        client.auth = false
        return Promise.reject(error)
      })
    }
  })
})

export default client
