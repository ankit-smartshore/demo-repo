'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'Patients',
        'DoctorsId', 
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'Doctors',
            key: 'id',
          }
        }
      ).then(()=>{
        return queryInterface.addColumn(
          'PatientDetails',
          'PatientId', {
            type: Sequelize.INTEGER,
            references: {
              model: 'Patients',
              key: 'id',
            }
          }
        )
      })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
