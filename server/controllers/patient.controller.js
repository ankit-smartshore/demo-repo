const db = require("../models");
const PatientDb = db.Patients;
const Op = db.Op;

// Create and Save a new Patient
exports.create = (req, res) => {
  // Validate request
  if (!req.body.email) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Patient
  const patient = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    active: req.body.active ? req.body.active : false,
    enrolled: req.body.enrolled ? req.body.enrolled : false,
    enrollDate: req.body.enrollDate,
  };

  // Save Patient in database
  PatientDb.create(patient)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err || "Some error occurred while creating the Patient."
      });
    });
};

// Retrieve all Patients from the database.
exports.findAll = (req, res) => {
  let doctorId = req.query.doctor;
  let isActive = req.query.active;

  let where = {
    DoctorsId: doctorId,
    active: isActive
  }

  const filteredWhere = Object.fromEntries(Object.entries(where).filter(([key, value]) => value !=null))

  PatientDb.findAll({
      where: filteredWhere,
      include: [{
          model: db.Doctors,
          as: 'doctor',
          attributes: {
            include: ['id', 'firstName', 'lastName', 'email'],
            exclude: ['password']
          }
        },
      {
        model: db.PatientDetails,
        as: 'patientdetail',
      }]
    })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log("eroorerror", err)
      res.status(500).send({
        message: err
      });
    });
};

// Find a single Patient with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  PatientDb.findByPk(id, {
      include: [
        { model: db.Doctors, as: 'doctor', attributes: { exclude: ['password'] }},
        { model: db.PatientDetails, as: 'patientdetail'}
      ]
    })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: `Error retrieving Patient with id = ${id} , ${err}`
      });
    });
};

// Update a Patient by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  PatientDb.update(req.body, {
      where: {
        id: id
      }
    })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Patient was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Patient with id=${id}. Maybe Patient was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Patient with id=" + id
      });
    });
};

// Delete a Patient with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  PatientDb.destroy({
      where: {
        id: id
      }
    })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Patient was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Patient with id=${id}. Maybe Patient was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Patient with id=" + id
      });
    });
};

// Delete all Patients from the database.
exports.deleteAll = (req, res) => {
  PatientDb.destroy({
      where: {},
      truncate: false
    })
    .then(nums => {
      res.send({
        message: `${nums} Patients were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while removing all Patients."
      });
    });
};

// Find all Active Patients
exports.findAllActive = (req, res) => {
  PatientDb.findAll({
      where: {
        active: 1
      }
    })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving Patients."
      });
    });
};

// Find all InActive Patients
exports.findAllInActive = (req, res) => {
  PatientDb.findAll({
      where: {
        active: 0
      }
    })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving Patients."
      });
    });
};